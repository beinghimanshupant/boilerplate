import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';

import HomePage from '../../containers/HomePage1/loadable'
import SideBar from '../../Components/SideBar'
import BuyContainer from '../../containers/Buy/loadable'
import SellContainer from '../../containers/Sell/Loadable'
//import HomePage from '../../containers/HomePage/Loadable';
//import FeaturePage from '../../containers/FeaturePage/Loadable';
//import NotFoundPage from '../../containers/NotFoundPage/Loadable';
const routes = [
    {
        path: '/',
        exact: true,
        main: props => <HomePage {...props} />,
    },
    {
        path: '/dashboard',
        exact: true,
        main: props => <HomePage {...props} />,
    },
    {
        path: '/buy',
        exact: true,
        main: props => <BuyContainer {...props} />,
    },
    {
        path: '/sell',
        exact: true,
        main: props => <SellContainer {...props} />,
    }
]

export default class Routes extends React.Component {
    debugger
    render() {
        return (
            <Row>
                <Col>
                <SideBar/>
                    <div>
                        <Switch>
                            {routes.map((route, index) => (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact={route.exact}
                                    render={route.main}
                                />
                            ))}
                        </Switch>
                    </div>
                </Col>
            </Row>
        );
    }
}