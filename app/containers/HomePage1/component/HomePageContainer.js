import React from 'react';
import HomePageRender from '../component/HomePageRender'

class HomePageContainer extends React.Component {
    render() {
        return (
            <HomePageRender />
        )
    }
}

export default HomePageContainer