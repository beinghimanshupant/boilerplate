/**
 * Asynchronously loads the component for HomePage
 */

 import Loadable from 'react-loadable'
//import HomePageContainer from './component/HomePageContainer';

 export default Loadable({
     loader : () => import('./component/HomePageContainer'),
     loading : () => null
 })