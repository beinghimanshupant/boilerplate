import { createSelector } from 'reselect';
import * as constants from './constants';

const selectDashboard = state => state.get(constants.NAME);
const selectData = () =>

  createSelector(selectDashboard, dashboardState =>
    dashboardState.get("tempData"),
  );

  export {selectData}