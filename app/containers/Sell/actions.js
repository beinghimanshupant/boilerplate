import * as types from './actionTypes';
import * as constants from './constants';

export function saveFormData(payload) {
  return {
    type: types.SAVE_FORM_DATA,
    payload
  };
}

export function getData(){
  return{
    type: types.GET_DATA
  }
}