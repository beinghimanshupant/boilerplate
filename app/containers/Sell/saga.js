import * as actions from './actions';
import * as types from './actionTypes';
import {
  put,
  takeEvery,
  call,
  take,
  all,
  takeLatest,
} from 'redux-saga/effects';

let response={"name":"Himanshu"}
function* getData(action){
    yield put(actions.saveFormData(response));
}

export default function* watchVetSagas() {
    debugger
    yield all([
      yield takeLatest(
        types.GET_DATA,
       getData,
      )])
    }