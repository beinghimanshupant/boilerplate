import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as selectors from '../selector'
import * as actions from '../actions'
import SellRender from '../component/SellRender'

class SellContainer extends React.Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
     this.props.actions.getData();
    }
    componentDidUpdate(prevProps){
        console.log(this.props.tempData)
    }

    render() {
        return (
            <div>
  <SellRender
  tempData={this.props.tempData}/>
    </div>
        )
    }
}

const makeMapStateToProps = () =>{
    const selectData = selectors.selectData()
    const mapStateToProps = state =>({
tempData : selectData(state)
    })
return mapStateToProps;
}

function mapDispatchToProps(dispatch) {
    const actionsToBind = Object.assign({}, actions);
    return {
      actions: bindActionCreators(actionsToBind, dispatch),
    };
  }
  export default connect(
    makeMapStateToProps,
    mapDispatchToProps,
  )(SellContainer);
