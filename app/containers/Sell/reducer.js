import { fromJS } from 'immutable';
import * as types from './actionTypes';
import * as constants from './constants';
const initialState = fromJS({});

function reducer(state = initialState, action) {
  switch (action.type) {
    case types.SAVE_FORM_DATA:
      return state.set("tempData",action.payload);
    default:
      return state;  }
}

export default reducer;