import { all } from 'redux-saga/effects';
import sellSaga from '../app/containers/Sell/saga'

export default function* rootSaga() {
    yield all([
        sellSaga()
    ])
}